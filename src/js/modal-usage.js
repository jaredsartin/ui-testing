/**
 *  Modal Usage JavaScript
 *  This file has usage and binding for the Modal Implementation
 */

 console.log('Loading user code for modal examples');

/**
 * Bind Buttons for interaction on the page
 */
 function bindButtons() {
   let basicButton = document.querySelector('#basic');
   basicButton.addEventListener('click', () => {
     ModalHandler.showModal({
       message: 'Simple Modal - Thanks for opening me. I have a couple callbacks, please check the console for each button and see differen messages.',
       onOK: () => console.log('Ok clicked - simple modal'),
       onCancel: () => console.log('Cancel clicked - simple modal'),
       onClose: () => console.log('Close clicked - simple modal'),
     });
   })

   let basicDarkButton = document.querySelector('#basic-dark');
   basicDarkButton.addEventListener('click', () => {
     ModalHandler.showModal({
       message: 'Simple Dark Modal - Thanks for opening me. I have NO custom callbacks for my buttons. I do demonstrate a basic styling - a dark variant of the base modal. This can be set to anything ans targeted with user CSS.',
       theme: 'dark', // This can be anything! Allows custom user styles, as it adds a class to the modal
     });
   })

   let advancedButton = document.querySelector('#advanced');
   advancedButton.addEventListener('click', () => {
     ModalHandler.showModal({
       title: 'Paragraphs and scrolling long text',
       message: [
         'You can choose to enable or disable any of the buttons, also we have ability to scroll when the text gets really looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong we break words where we need to and handle lots of lorem.',
         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed luctus dui, et rutrum tortor. Donec tempor rutrum odio ac luctus. Nulla eleifend lorem nisl, vitae bibendum augue scelerisque at. Donec condimentum magna quis risus tincidunt, sit amet eleifend massa tempus. Quisque aliquam massa eu placerat tristique. Vivamus hendrerit nec enim vel ultrices. Donec in tellus vitae ante tempus iaculis. Vestibulum vestibulum condimentum felis, et mollis est aliquet eu. Phasellus malesuada tellus tellus, in ultricies orci maximus a. Cras magna mauris, pharetra sit amet imperdiet eu, blandit sit amet leo.',
         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed luctus dui, et rutrum tortor. Donec tempor rutrum odio ac luctus. Nulla eleifend lorem nisl, vitae bibendum augue scelerisque at. Donec condimentum magna quis risus tincidunt, sit amet eleifend massa tempus. Quisque aliquam massa eu placerat tristique. Vivamus hendrerit nec enim vel ultrices. Donec in tellus vitae ante tempus iaculis. Vestibulum vestibulum condimentum felis, et mollis est aliquet eu. Phasellus malesuada tellus tellus, in ultricies orci maximus a. Cras magna mauris, pharetra sit amet imperdiet eu, blandit sit amet leo.',
        ],
       showCancel: false,
       showOK: false,
     });
   })

   let stackingButton = document.querySelector('#stacking');
   stackingButton.addEventListener('click', () => {
     ModalHandler.showModal({
       title: 'Stacking Demo',
       theme: 'dark',
       message: 'You will see multiple modals.',
       showCancel: false,
       showClose: false,
     });
     ModalHandler.showModal({
       message: 'This modal can either go away by clicking ok or close. Clicking cancel will trigger another modal after this!',
       onCancel: () => {
        ModalHandler.showModal({
          message: 'Told you it would show up if you clicked cancel!',
        });
       }
     });
   })

   let controlledButton = document.querySelector('#code-control');
   controlledButton.addEventListener('click', () => {
     ModalHandler.showModal({
       message: 'This message will self distruct in 6 seconds...',
       showCancel: false,
       showOK: false,
       showClose: false,
     });

     setTimeout(() => ModalHandler.hideOpenModal(), 6000);
   });
 }

 bindButtons();