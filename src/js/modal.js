/**
 * A song
 * @typedef {Object} ModalOptions
 * @property {string} [title] - Optional Title
 * @property {string|array} message - Modal message
 * @property {string} [theme] - Dark or Light theme
 * @property {boolean} [showClose] - Show close button on top right
 * @property {boolean} [showOK] - Show OK button on bottom right
 * @property {boolean} [showCancel] - Show cancel button on bottom right
 * @property {function} [onClose] - Action taken when close is clicked
 * @property {function} [onOK] - Action taken when OK is clicked
 * @property {function} [onCancel] - Action taken when Cancel is clicked
 */

const BASE_OPTIONS = {
  theme: 'light',
  showCancel: true,
  showClose: true,
  showOK: true,
}

class ModalHandler {
  constructor() {
    this.currentModal = null;
    this.modalQueue = [];
    this.createModalStructure();
  }

  /**
   * Shows a modal with the given options
   * @param {ModalOptions} modalOptions 
   */
  static showModal(modalOptions) {
    ModalHandlerInstance.show(modalOptions);
  }

  /**
   * Hides the open modal
   */
  static hideOpenModal() {
    ModalHandlerInstance.hideOpen();
  }

  /**
   * Internal: Show or Queue modal
   * @param {ModalOptions} modalOptions 
   */
  show(modalOptions) {
    if(modalOptions && !this.currentModal) {
      this.buildAndShow(modalOptions)
    } else {
      this.modalQueue.push(modalOptions)
    }
  }

  /**
   * Internal: Build modal structure and apply show animations
   * @param {ModalOptions} modalOptions 
   */
  buildAndShow(modalOptions) {
    // Clear old modal
    this.clearModalContent();

    this.currentModal = {...BASE_OPTIONS, ...modalOptions};

    if(this.currentModal.title) {
      let modalTitle = document.createElement('h3');
      modalTitle.innerText = this.currentModal.title;
      this.modal.appendChild(modalTitle);
    }

    if(Array.isArray(this.currentModal.message)) {
      this.currentModal.message.forEach((msg) => {
        let modalMessage = document.createElement('p');
        modalMessage.innerText = msg;
        this.modalContent.appendChild(modalMessage);
      })
    } else {
      let modalMessage = document.createElement('p');
      modalMessage.innerText = this.currentModal.message;
      this.modalContent.appendChild(modalMessage);
    }

    this.modal.appendChild(this.modalContent);

    if(this.currentModal.showClose) this.modal.appendChild(this.modalClose);

    if(this.currentModal.showOK || this.currentModal.showCancel) {
      this.modal.appendChild(this.modalBottomActions)
      if(this.currentModal.showOK)
        this.modalBottomActions.appendChild(this.modalOK);
      if(this.currentModal.showCancel)
        this.modalBottomActions.appendChild(this.modalCancel);
    }

    // Set the CSS animation to run 2ish frames later
    // Animations don't fire when className + append happen in same tick
    setTimeout(() => {
      this.shade.classList.add('show');
      this.modal.className = 'show'; // Resets theming

      if(this.currentModal.theme)
        this.modal.classList.add(this.currentModal.theme);
    }, 30);
  }

  /**
   * Internal: Removes elements from modal container
   * Additionally removes nested content
   */
  clearModalContent() {
    while(this.modal.firstChild) {
      this.modal.removeChild(this.modal.firstChild)
    }
    while(this.modalBottomActions.firstChild) {
      this.modalBottomActions.removeChild(this.modalBottomActions.firstChild)
    }
    while(this.modalContent.firstChild) {
      this.modalContent.removeChild(this.modalContent.firstChild)
    }
  }

  /**
   * Internal: Hides the open modal with type of close event for callback
   * @param {string} [closeType]
   */
  hideOpen(closeType=null) {
    if(closeType === 'ok' && this.currentModal.onOK)
      this.currentModal.onOK();
    if(closeType === 'cancel' && this.currentModal.onCancel)
      this.currentModal.onCancel();
    if(closeType === 'close' && this.currentModal.onClose)
      this.currentModal.onClose();

    if(this.currentModal) {
      this.modal.classList.remove('show');
      setTimeout(() => {
        this.currentModal = null;
        if(this.modalQueue.length) {
          this.buildAndShow(this.modalQueue.shift());
        } else {
          this.shade.classList.remove('show');
        }
      }, 220)
    }
  }

  /** 
   * Internal: Create Modal Structure
   * ONLY CREATED ONCE
   * Using document.createElement:
   * - we can ensure saftey over unsafe innerHTML sets
   * - programatically support things like multiple paragraph messages
   * - handle optional elements better
   */
  createModalStructure() {
    this.shade = document.createElement('div');
    this.shade.id = 'modal-shade';

    this.modal = document.createElement('div');
    this.modal.id = 'modal-window';
    this.shade.appendChild(this.modal);

    this.modalBottomActions = document.createElement('div');
    this.modalBottomActions.id = 'modal-bottom-actions';

    this.modalContent = document.createElement('div');
    this.modalContent.id = 'modal-content';

    this.modalClose = document.createElement('div');
    this.modalClose.id = 'modal-close';
    this.modalClose.innerText = '\u2715';
    this.modalClose.addEventListener('click', () => this.hideOpen('close'))

    this.modalCancel = document.createElement('button');
    this.modalCancel.id = 'modal-cancel';
    this.modalCancel.innerText = 'Cancel';
    this.modalCancel.addEventListener('click', () => this.hideOpen('cancel'))

    this.modalOK = document.createElement('button');
    this.modalOK.id = 'modal-ok';
    this.modalOK.innerText = 'Ok';
    this.modalOK.className = 'primary';
    this.modalOK.addEventListener('click', () => this.hideOpen('ok'))

    document.body.appendChild(this.shade);
  }
}

const ModalHandlerInstance = new ModalHandler();