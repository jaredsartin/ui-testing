class CircleAnimator {
  constructor() {
    this.circles = [
      document.querySelector('#red'),
      document.querySelector('#green'),
      document.querySelector('#blue'),
      document.querySelector('#yellow'),
    ]
    this.container = document.querySelector('#container');
    this.animationName = document.querySelector('#animation-name');

    this.resetAndPlay();

    document.querySelector('#reset').addEventListener('click', this.resetAndPlay.bind(this));
    document.querySelector('#finish').addEventListener('click', this.finishAnimation.bind(this));
  }

  /**
   * Main entry to starting animations over
   */
  resetAndPlay() {
    this.resetStage();
    setTimeout(() => this.randomizeAnimation(), 35);
  }

  /**
   * Resets the stage, removing classes
   */
  resetStage() {
    this.container.className = 'kill-animations';
    this.circles.forEach((cir) => cir.classList.remove(
      'order-0',
      'order-1',
      'order-2',
      'order-3',
    ));
  }

  /**
   * Sets up the animation
   * - Randomizes the order of circles
   * - Chooses random animation
   */
  randomizeAnimation() {
    const animationClass = shuffleArray([
      'centered-enter',
      'corner-enter',
      'drop-enter'
    ])[0];

    this.animationName.innerText = animationClass;
    this.container.className = animationClass;

    let shuffled = shuffleArray(this.circles);
    shuffled.forEach((cir, i) => cir.classList.add(`order-${i}`))

    setTimeout(() => this.container.classList.add('active'), 35);
    // this.container.classList.add('active');
  }

  /**
   * Complete the animation immediately
   * - shows all circles
   * - stops all timers
   */
  finishAnimation() {
    this.container.classList.add('kill-animations');
  }
}

/**
 * Copy and shuffle an array
 * @param {Array} arr array to be shuffled
 * @returns {Array} shuffledArray
 */
function shuffleArray(arr) {
  const newArr = arr.slice();
  for (let i = newArr.length - 1; i > 0; i--) {
    const rand = Math.floor(Math.random() * (i + 1));
    [newArr[i], newArr[rand]] = [newArr[rand], newArr[i]];
  }
  return newArr;
};

new CircleAnimator();