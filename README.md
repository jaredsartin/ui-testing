# UI Challenge #

This code lives at https://bitbucket.org/jaredsartin/ui-testing/src/master/

Readme is best opened from that URL or an editor with Markdown support / linewrapping.

The Trello board for ideation and task tracking can be found [here](https://trello.com/b/DOAzIPhs/challenge-tracker).

## Getting Started ##

`git clone git@bitbucket.org:jaredsartin/ui-testing.git`

Open `src/index.html` from your file browser or start a [simple server](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server) on the `src` directory and visit the corresponding `localhost` link.

## What is this for? ##

This is a small project used for an interview technical challenge. The company is not named and the challenges are re-written in my own words to prevent searching and re-use of my code. The challenge was given to complete over the course of a week, and will contain multiple commits to show code style, thought pattern and progression. If you are viewing this as a download, without the history, please visit the link at the top to see more details.

## The Challenges ##

There are two challenges posed for this assignment. The constraints to the project are to not use any libraries or helper code. I will also take this to mean not using something like a bundler or builder - forgoing use of Webpack or Parcel. I will be hand coding all files in src and additionally skipping use of any babel or transpiling tools like less/sass/stylus. VSCode will be used to complete thechallenge, but I do not have any extensive linters or helper scripts, mostly a bare bones installation + vim plugins.

### Circles ###

The goal of this exercise is to show circles on-screen based on a timer. When coming to the page, only a `Reset` and `Finish` button are seen. For each circle, of colors red, blue, yellow, and green, display them on-screen; circles should appear one-by-one on a 2 second timer. The ordering of their appearance is to be randomized and the circle should remain on-screen until the experience is reset.

#### Interaction ####

* Clicking the reset button will clear all circles from the screen, reset all timers and random selection of order - then begin showing the circles on the 2 second timer
* Clicking finish should stop all timers and display the circles immediately

### Modal ###

The goal of this exercise is to create a reusable modal, without using alert. The modal is to be centered in the viewport, prevent interaction with the rest of the page, and contain the following elements:

* Close button in the top-right corner of the modal
* Okay and Cancel buttons in the bottom-right of the modal

The actions taken from the interactive elements should be dynamic, the message displayed in the modal should be dynamic as well.

## The Log ##

A log of my work has been kept below, this will notate things like time spent, assumptions, and have any open questions or next steps. It should reflect things that would normally be covered in text chat, email, standups, or quick team meetings.

### Journal ###

* **Thu 23 Jul - Roughly 60 minutes** - Initial project setup and ideation for solutions. Will create variants of the circles exercise for things like: responsive viewport corner animations, "smear frame" style entry with skewing, and anything else I think would be a fun animation. The approach to handling the modal will be to get a basic modal in place, then add support for callbacks, dynamic text, optional title, dynamic sizing, color, and anything additional that may seem relevant to a core modal use. Created basic tickets in Trello.

* **Sun 26 Jul - Roughly 3 hours** - I took to making the basic modal implementation, adding a handful of nice to haves. The reasoning for the extras is that a modal is never just a simple text display with callbacks. I also had a ton of fun building out the options for the modal and customization. The styles aren't amazing, but there is some animation on the show and hide of the modal.

* **Mon 27 Jul - Roughly 90 minutes** Created basic framework for circle animations, not creating the finish button. Current animation shows the circles entering with an SVG animation. The circles animate in random order but in a known pattern. Planning to finish the "finish" button and add a couple variants on the animations.

* **Tue 28 Jul - Roughly 90 minutes** While I didn't create all the animations for the circles I had intended on, I came up with new ones based on some additional thought on web rendering capabilities. A stroke animation that rotates all four circles in the center, a drop in animation where the balls drop in, and one where they all come in on different corners. The drop and stroke animations go left to right, choosing different colors for circles 1-4, and the corner colors stay in the same spot, but animat in at a different order. The reset button will choose a random animation.

**Total estimated time for challenges, setup, and journaling:** Roughly 7 hours

### Next Steps ###

* Not all viewports are created equal - project assumes that we are in a non-scrolling environment, such as a Game UI.
* Assumptions made here are to use DOM elements for rendering - some additional animation and effects would be better suited in Canvas.
* While I made my best judgment calls on the current output of the solutions, I think it would be fair to open the code and the approaches up to a broader team - get some feedback on how the tools would fit, ensure the design approaches align with the visual language, and such.
* REFACTOR! Lots of assumptions made and hard coded. I would move to Sass (or similar) to allow stronger variable handling (base CSS allows this, but limited support) for timers and calculations. Brigning in a bundler system would allow extrapolation of utilities, like `document.querySelector` or other functionality into smaller, reusable tooling.

### Opens ###

* Normally the UI/UX/Motion teams would have input on the type of animation for the circles, I would consult them for other placement and animations.
* The modal is rich, but not rich enough. Thinking about modals with links (web) or imagery that augments the text would mean some more design and code design to tackle. Would investigate potential usage of the modal I built, in order to derive next steps.
* Most of the circle solutions assume the circle is what we are animating (taking some creative liberties in their appearance) - what happens when this ask evolves to new shapes or richer UI elements?